﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ProgrammingProject3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void openFileButton_Click(object sender, EventArgs e)
        {
            StreamReader inputFile;
            StreamWriter outputFile;
            

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                //Declare file reader and file writer objects
                inputFile = File.OpenText(openFile.FileName);  
                outputFile = File.CreateText("output.txt");
                
                
                

                //Declare variables for keeping track of target strings
                String largest = "";
                String mostVowels = "";
                String currentString = "";
                String alphaFirst = "";
                String alphaLast = "";
                bool breakOccur = false;
                int shorterLength;
                bool firstRun = true;
                int currentStringVowels = 0;
                int mostVowelsInt = 0;
                while (!inputFile.EndOfStream) 
                {
                    currentString = inputFile.ReadLine();
                    currentString = currentString.ToLower();
                    //Largest
                    if (currentString.Length > largest.Length) 
                    {
                        largest = currentString;
                    }

                    //Check vowel amount
                    for (int i = 0; i < currentString.Length; i++) 
                    {
                        if (currentString[i] == 'a' || currentString[i] == 'e' || currentString[i] == 'i' || currentString[i] == 'o' || currentString[i] == 'u') 
                        {
                            currentStringVowels++;
                        }
                    }

                    //Compare amount of vowels to string that currently has most vowels
                    if (currentStringVowels > mostVowelsInt) 
                    {
                        mostVowelsInt = currentStringVowels;
                        mostVowels = currentString;
                    }

                    //Reset vowel counter
                    currentStringVowels = 0;

                    //Write to output
                    outputFile.WriteLine(currentString);
                    

                    //Alphatbetize
                    if (firstRun)
                    {
                        alphaFirst = currentString;
                        alphaLast = currentString;
                        firstRun = false;
                    }
                    else 
                    {
                        
                        //Find alphabetically first
                        if (currentString.Length > alphaFirst.Length)
                        {
                            shorterLength = alphaFirst.Length;
                        }
                        else 
                        {
                            shorterLength = currentString.Length;
                        }

                        for (int i = 0; i < shorterLength; i++) 
                        {
                            if (currentString[i] < alphaFirst[i])
                            {
                                alphaFirst = currentString;
                                breakOccur = true;
                                break;
                            }
                            else if (currentString[i] > alphaFirst[i]) 
                            {
                                breakOccur = true;
                                break;
                            }
                        }
                        if (!breakOccur) 
                        {
                            if (currentString.Length == shorterLength) 
                            {
                                alphaFirst = currentString;
                            }
                        }
                        breakOccur = false;

                        //Find alphabetically last
                        if (currentString.Length > alphaLast.Length)
                        {
                            shorterLength = alphaLast.Length;
                        }
                        else
                        {
                            shorterLength = currentString.Length;
                        }

                        for (int i = 0; i < shorterLength; i++)
                        {
                            if (currentString[i] > alphaLast[i])
                            {
                                alphaLast = currentString;
                                breakOccur = true;
                                break;
                            }
                            else if (currentString[i] < alphaLast[i])
                            {
                                breakOccur = true;
                                break;
                            }
                        }
                        if (!breakOccur)
                        {
                            if (alphaLast.Length == shorterLength)
                            {
                                alphaLast = currentString;
                            }
                        }
                        breakOccur = false;
                    }
                }
                displayOutputLabel.Text = "Largest: " + largest + ", Most Vowels: " + mostVowels + ", Alphabetically First: " + alphaFirst + ", Alphabetically Last: " + alphaLast;
                inputFile.Close();
                outputFile.Close();


            }
            else
            {
                MessageBox.Show("No file was selected");
            }
        }

        //Clears text from displayOutputLabel.
        private void clearButton_Click(object sender, EventArgs e)
        {
            displayOutputLabel.Text = "";
        }
    }
}
